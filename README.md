# mr-robot

MR Camera Streaming - Windows
==============================

The Windows VIsual Studio 2017 solution is in cv_win\mrcam.

Prerequisites:
* Install Visual Studio 2017 to the standard loaction 
* Install Windows SDK to the standard location
* Install Commn Vision Blox SDK to the standard location

Compilation:
* Clone the repo
* Open cv_win\mrcam\mrcam.sln in Visual Studio
* Compile 64 bit Debug version

Running:
* Ensure the camera is plugged in to the computer and test it is properly accessible
for image streamig using CVB GenICamBrowser application.
* Close down the CVB application properly to free the handle to the camera.
* Open a cmd window
* Navigate to mr-robot-camera\cv_win\mrcam\x64\Debug\ directory
* A pre-compiled 64-bit executable has been checked into Git within this directory,
but you should be able to compile your own and overwrite it.
* Run
> mrcam.exe
* The program creates a %HOMEPATH%\data\ directory directly under the user's home directory 
and stores (for now) 8bit PNG monochrome images in it.
* Enter Ctrl-C to exit.
* The program exits, shutting down the camera properly.
* NOTE: The last image written is likely half captured and needs to be discarded.

