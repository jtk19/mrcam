// mrcam.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <windows.h>
#include <iostream>
#include <exception>
#include <vector>
#include <string>
#include <iostream>
#include <iomanip>
#include <stdio.h>
#include <stdlib.h>
#include <direct.h>
#include <iCVCImg.h>
#include <iCVCDriver.h>
#include <iCVCUtilities.h>
#include <iCVGenApi.h>
#include <CVCError.h>
#include <cvb/device_factory.hpp>
#include <cvb/driver/stream.hpp>
#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>

#define	SIZE_MAX	65500

using namespace std;
using namespace Cvb::V_1_3;


const string HOME_PATH = getenv("HOMEPATH");
const string DATA_PATH = HOME_PATH + "\\data\\";

static const size_t DRIVERPATHSIZE = 256;
static const size_t FNAMESIZE = 512;

size_t IMG_HEIGHT = 1200;
size_t IMG_WIDTH = 1920;


void init_system();
void scan_for_cameras();
void genicam_access(IMG hCamera);


IMG hCamera = NULL;

// terminating cleanup
BOOL WINAPI consoleHandler(DWORD signal)
{
	if (signal == CTRL_C_EVENT)
	{
		cout << "Free camera" << endl;
		ReleaseObject(hCamera);
	}
	return TRUE;
}


int main( int argc, char *argv[] )
{
    std::cout << "Welcome to MRCam!\n";

	init_system();

	scan_for_cameras();

	char driverPath[DRIVERPATHSIZE] = { 0 };
	TranslateFileName("%CVB%\\Drivers\\GenICam.vin", driverPath, DRIVERPATHSIZE);
	
	cvbbool_t success = LoadImageFile(driverPath, hCamera);
	if (!success)
	{
		cout << "Error loading \"" << driverPath << "\" driver!" << endl;
		cout << "Probably no cameras were found during discovery scan!" << endl;
		return -1;
	}
	cout << "[INFO] Loading driver \"" << driverPath << "\" successful." << endl;

	// Information about the camera
	if (CanNodeMapHandle(hCamera))
	{
		genicam_access(hCamera);
	}


	// Acquire images
	char fn[FNAMESIZE];
	cout << "Acquiring image frames." << endl;
	cvbres_t result = G2Grab(hCamera);
	size_t i = 0, j = 0;
	if (result >= 0)
	{
		// acquire 10 images
		cout << "Acquiring images . . . Do Ctrl-C to end." << endl;
		while (true)
		{
			// wait for next image to be acquired
			// (returns immediately if unprocessed image are in the ring buffer)
			result = G2Wait(hCamera);
			if (result < 0)
			{
				cout << setw(4) << i << " Error with G2Wait: " << CVC_ERROR_FROM_HRES(result) << endl;
			}
			else
			{
				// do image processing
				void *pBase = NULL;
				intptr_t xInc = 0;
				intptr_t yInc = 0;
				GetLinearAccess(hCamera, 0, &pBase, &xInc, &yInc);
				//cout << "Image x-increment " << xInc << " y-increment " << yInc << endl;
				//cout << setw(4) << i << " Acquired image @" << hex << pBase << endl;
				cv::Mat m(IMG_HEIGHT, (int)yInc, 0, pBase);
				sprintf( fn, "%sfr_%06zu_%06zu.png", DATA_PATH.c_str(), j, i);
				cv::imwrite( fn, m);
				if (++i >= SIZE_MAX)
				{
					i = 0;
					++j;
				}
			}
		}

		std::cout << "Stop acquisition" << endl;
		// stop the grab (kill = false: wait for ongoing frame acquisition to stop)
		result = G2Freeze(hCamera, true);
	}
	else
	{
		cout << "Error starting acquisition!" << endl;
	}

	// free camera
	cout << "Free camera" << endl;
	ReleaseObject(hCamera);

	return 0;
}


void scan_for_cameras()
{
	char iniPath[DRIVERPATHSIZE] = { 0 };
	TranslateFileName("%CVBDATA%\\Drivers\\GenICam.ini", iniPath, DRIVERPATHSIZE);

	cout << "Configuring driver to scan for cameras . . . ";
	BOOL result = WritePrivateProfileStringA("SYSTEM", "CreateAutoIni", "1", iniPath);
	cout << (result ? "successful" : "error") << endl;
	cout << "Init path: " << iniPath << endl << endl;
}

// Access config data via CVGenApi
void genicam_access(IMG hCamera)
{
	cout << endl;
	NODEMAP hNodeMap = NULL;
	cvbres_t result = NMHGetNodeMap(hCamera, hNodeMap);
	if (result >= 0)
	{
		// get width feature node
		NODE hHeight = NULL;
		result = NMGetNode( hNodeMap, "Height", hHeight);
		if (result >= 0)
		{
			// value camera dependent
			cvbint64_t heightValue = 0;
			result = NGetAsInteger(hHeight, heightValue);
			// set values via NSetAsInteger( hHeight, heightValue);
			if (result >= 0)
			{
				cout << "Camera image height : " << heightValue << endl;
				IMG_HEIGHT = heightValue;
			}
			else
			{
				cout << "Node value error getting height: " << CVC_ERROR_FROM_HRES(result) << endl;
			}
		}


		NODE hWidth = NULL;
		result = NMGetNode(hNodeMap, "Width", hWidth);
		if (result >= 0)
		{
			cvbint64_t widthValue = 0;
			result = NGetAsInteger(hWidth, widthValue);
			// set values via NSetAsInteger(hWidht, widthValue);
			if (result >= 0)
			{
				cout << "Camera image width : " << widthValue << endl;
				IMG_WIDTH = widthValue;
			}
			else
			{
				cout << "Node value error getting width: " << CVC_ERROR_FROM_HRES(result) << endl;
			}

			ReleaseObject(hWidth);
		}
		else
		{
			cout << "Node error: " << CVC_ERROR_FROM_HRES(result) << endl;
		}
		ReleaseObject(hNodeMap);
	}
	else
	{
		cout << "NodeMap error: " << CVC_ERROR_FROM_HRES(result) << endl;
	}
}

void init_system()
{
	std::error_code errorCode;
	string cmd = string("del /f /q /s ") + DATA_PATH + "*.* > NULL";
	string cmd1 = string("rmdir /q /s ") + DATA_PATH;
	system(cmd.c_str()) ;
	system(cmd1.c_str());

	cmd = string("mkdir ") + DATA_PATH;
	system(cmd.c_str());
}